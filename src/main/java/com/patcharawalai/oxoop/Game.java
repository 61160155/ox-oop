/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patcharawalai.oxoop;

import java.util.Scanner;

/**
 *
 * @author Patcharawalai
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int Row, Col;
    Scanner sc = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            Row = sc.nextInt() - 1;
            Col = sc.nextInt() - 1;

            if (table.setRowCol(Row, Col)) {
                break;
            }
            System.out.println("Error: table at row and col is not available.");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        int rand =((int) (Math.random()*100))%2;
        if (rand == 0){
            table = new Table(playerX, playerO);
        }else{
            table = new Table(playerO, playerX);
        }  
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                this.showTable();
                System.err.println(" ");
                newGame();
            }
            table.switchPlayer();
        }
    }
}
